package gov.dhs.kudos.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import gov.dhs.kudos.model.*;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import gov.dhs.kudos.model.forms.KudosForm;

@Controller
public class KudosController extends BaseController {

	private static Logger LOGGER = LoggerFactory.getLogger(KudosController.class);

	@Autowired
	private SimpMessagingTemplate template;

	private final TaskScheduler scheduler = new ConcurrentTaskScheduler();

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(final HttpServletRequest request, final ModelMap model) {
		String page = "redirect:/userProfile"; // until we convert all pages such as admin and supervisor landing

		final Collection<? extends GrantedAuthority> authorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		if (authorities.contains(new SimpleGrantedAuthority("ROLE_EMPLOYEE"))) {
			page = "redirect:/landing/employee";
		}

		return page;
	}

	@RequestMapping(value = "/orgchart", method = RequestMethod.GET)
	public String orgchart(final ModelMap model) {
		model.addAttribute("user", getPrincipal());
		model.addAttribute("allusers", userRepo.findAll());
		return "orgchart";
	}

	@RequestMapping(value = "/landing/employee", method = RequestMethod.GET)
	public String employeeLanding(final ModelMap model) {
		model.addAttribute("employee", getPrincipal());

		// my kudos list
		final Employee employee = getLoggedInUser();
		if (employee.getKudos() != null) {
			model.addAttribute("myKudos", employee.getKudos());

			// calculate my ranking
			final Ranking ranking = calcRanking(employee);
			model.addAttribute("ranking", ranking.getRanking());
			model.addAttribute("percentile33", ranking.getPercentile33());
			model.addAttribute("percentile66", ranking.getPercentile66());

			// employee info
			model.addAttribute("employeeObj", employee);
		} else {
			model.addAttribute("myKudos", new ArrayList<Kudos>());
		}
		model.addAttribute("allusers", userRepo.findAll());
		model.addAttribute("allProjects", projectRepo.findAll());
		return "employee/employeeLanding";
	}

	@RequestMapping(value = "/landing/supervisor", method = RequestMethod.GET)
	public String supervisorLanding(final ModelMap model) {
		// TODO: Add logic needed for content

		return "supervisor/supervisorLanding";
	}

	@RequestMapping(value = "/userProfile", method = RequestMethod.GET)
	public String userProfile(final ModelMap model) {
		model.addAttribute("employee", getPrincipal());

		final Employee employee = getLoggedInUser();
		if (employee.getKudos() != null) {
			model.addAttribute("myKudos", employee.getKudos());
		} else {
			model.addAttribute("myKudos", new ArrayList<Kudos>());
		}

		final Ranking ranking = calcRanking(employee);
		model.addAttribute("ranking", ranking.getRanking());
		model.addAttribute("percentile33", ranking.getPercentile33());
		model.addAttribute("percentile66", ranking.getPercentile66());

		model.addAttribute("allusers", userRepo.findAll());
		model.addAttribute("allProjects", projectRepo.findAll());
		return "userProfile";
	}

	@RequestMapping(value = "/kudos/giveKudos", method = RequestMethod.GET)
	public String giveKudos(final ModelMap model) {
		model.addAttribute("user", getPrincipal());
		model.addAttribute("allusers", userRepo.findAll());
		model.addAttribute("allProjects", projectRepo.findAll());
		return "kudos/giveKudos";
	}

	@RequestMapping(value = "/employee/processKudos", method = RequestMethod.POST)
	public String employeeKudos(@Valid @ModelAttribute("kudosForm") final KudosForm form, final BindingResult result, final ModelMap model) {
		saveKudos(getLoggedInUser(), userRepo.findOne(Long.valueOf(form.getForEmployee())),
				form.getKudos(), starRepo.findByType(form.getRating()), projectRepo.findOne(form.getProjectId()));

		model.addAttribute("user", getPrincipal());
		return "redirect:/landing/employee";
	}

	@RequestMapping(value = "/kudos/processKudos", method = RequestMethod.POST)
	public String registerUser(@Valid @ModelAttribute("kudosForm") final KudosForm form, final BindingResult result, final ModelMap model) {
		saveKudos(getLoggedInUser(), userRepo.findOne(Long.valueOf(form.getForEmployee())),
				form.getKudos(), starRepo.findByType(form.getRating()), projectRepo.findOne(form.getProjectId()));

		model.addAttribute("user", getPrincipal());

		return "redirect:/userProfile";
	}

	private void saveKudos(Employee byEmployee, Employee forEmployee, String kudo, StarRating starRating, Project project){
		final Kudos newKudos = new Kudos();
		newKudos.setByEmployee(byEmployee);
		newKudos.setForEmployee(forEmployee);
		newKudos.setKudos(kudo);
		newKudos.setStarRating(starRating);
		newKudos.setProject(project);
		kudosRepo.save(newKudos);
		kudosBroadcast();

	}

	private Ranking calcRanking(final Employee employee) {
		final List<Employee> allEmployees = userRepo.findAll();
		Collections.sort(allEmployees, Collections.reverseOrder((u1, u2) -> Integer.compare(u1.getKudos().size(), u2.getKudos().size())));
		final int n = allEmployees.size();
		final int ranking = allEmployees.indexOf(employee) + 1;
		final double rank33 = calcPercentileRank(n, 33);
		final double rank66 = calcPercentileRank(n, 66);
		return new Ranking(ranking, rank33, rank66);
	}

	private double calcPercentileRank(final int n, final double p) {
		final double rank = (p / 100) * (n + 1);
		return rank;
	}

	private void kudosBroadcast() {
		final Pageable pageable = createPageRequest();
		final List<Kudos> page = kudosRepo.findAll(pageable).getContent();
		final List<KudosFeedItem> feed = new ArrayList<>();
		CollectionUtils.collect(page, input -> new KudosFeedItem(input.getByEmployee().getFirstName(), input.getForEmployee().getFirstName(), input.getKudos()), feed);
		template.convertAndSend("/feed/kudos", feed);
	}

	private Pageable createPageRequest() {
		return new PageRequest(0, 10, Sort.Direction.DESC, "id");
	}

	/**
	 * Invoked after bean creation is complete, this method will schedule kudosBroadcast every 10 seconds
	 */
	@PostConstruct
	private void broadcastTimePeriodically() {
		scheduler.scheduleAtFixedRate(() -> kudosBroadcast(), 10000);
	}
}
