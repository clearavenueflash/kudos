package gov.dhs.kudos.model.forms;

import gov.dhs.kudos.model.StarRatingType;

public class KudosForm {

	private String kudos;
	private String forEmployee;
	private Long projectId;
	private StarRatingType rating;

	public String getKudos() {
		return kudos;
	}

	public void setKudos(final String val) {
		kudos = val;
	}

	public String getForEmployee() {
		return forEmployee;
	}

	public void setForEmployee(final String val) {
		forEmployee = val;
	}

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public StarRatingType getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = StarRatingType.valueOf(rating);
	}
}
