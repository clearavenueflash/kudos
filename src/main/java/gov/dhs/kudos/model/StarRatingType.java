package gov.dhs.kudos.model;

public enum StarRatingType {
	ZERO("ZERO"),
	ONE("ONE"),
	TWO("TWO"),
	THREE("THREE"),
	FOUR("FOUR"),
	FIVE("FIVE");

	String starRatingType;

	private StarRatingType(final String val) {
		starRatingType = val;
	}

	public String getRoleType() {
		return starRatingType;
	}

}