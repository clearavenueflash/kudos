package gov.dhs.kudos.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import gov.dhs.kudos.model.Project;

@Transactional
public interface ProjectRepository extends JpaRepository<Project, Long> {

}
