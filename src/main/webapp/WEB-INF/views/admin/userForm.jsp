<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>

<c:url value="/resources" var="resources" />
<c:url value="/admin/processUser" var="processUser" />
<c:url value="/admin" var="admin" />
<c:url value="/logout" var="logout" />



<!DOCTYPE html>
<!--[if lt IE 9]><html class="lt-ie9"><![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en">
<!--<![endif]-->
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="HandheldFriendly" content="True">
<meta name="MobileOptimized" content="320">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Admin Page</title>

<!--[if lt IE 9]>
	  <script src="${resources}/js/html5shiv.min.js"></script>
	<![endif]-->

<link href="${resources}/css/uswds.min.css" rel="stylesheet" />
<link href="${resources}/css/kudos.css" rel="stylesheet" />
</head>

<body>
	<a class="skipnav" href="#main-content">Skip to main content</a>
	<header class="usa-site-header" role="banner">
		<div class="site-navbar">
			<a class="menu-btn" href="#">Menu</a>
			<div class="site-logo" id="logo">
				<em>Users Menu</em>
			</div>
			<ul class="usa-button-list usa-unstyled-list">
				<li><a class="usa-button usa-button-outline-inverse" onclick="window.location.href = '${admin}'"> Cancel </a></li>
				<li><a class="usa-button usa-button-outline-inverse" onclick="window.location.href = '${logout}'"> Logout </a></li>
			</ul>
		</div>
	</header>
	<div class="main-content" id="main-content">
		<div class="kudos-content usa-content">
			<form:form class="usa-form" method="post" action="${processUser}" modelAttribute="userForm">
				<fieldset>
					<legend>User</legend>
					<label for="email">Email <span class="usa-additional_text">Required</span></label>
					<input id="email" name="email" type="email" required="" aria-required="true" value="${modifyEmployee.email}">
					<label for="firstName">First name <span class="usa-additional_text">Required</span></label> 
					<input id="firstName" name="firstName" type="text" required="" aria-required="true" value="${modifyEmployee.firstName}">
					<label for="lastName">Last name <span class="usa-additional_text">Required</span></label> 
					<input id="lastName" name="lastName" type="text" required="" aria-required="true" value="${modifyEmployee.lastName}">
					<label for="password">Password <span class="usa-additional_text">Required</span></label> 
					<input id="password" name="password" type="password" required="" aria-required="true" <c:if test="${modifyEmployee != null}"><c:out value="disabled='disabled'"/></c:if> >
					<label for="confirmPassword">Confirm Password <span class="usa-additional_text">Required</span></label>
					<input id="confirmPassword" name="confirmPassword" type="password" required="" aria-required="true" <c:if test="${modifyEmployee != null}"><c:out value="disabled='disabled'"/></c:if> >
					
					<input id="id" name="id" type="hidden" value="${modifyEmployee.id}">
					
					<label for="role">Role <span class="usa-additional_text">Required</span></label>
					<select name="role" id="role">
						<c:forEach var="r" items="${allroles}">
							<option value="${r.id}" <c:if test="${modifyEmployee.role.id == r.id}"><c:out value="selected='selected'"/></c:if> >${r.type}</option>
						</c:forEach>
					</select> 

					<label for="manager1">Manager1</label>
					<select name="manager1" id="manager1">
						<option value="">NONE</option>
						<c:forEach var="u" items="${allusers}">
							<option value="${u.id}" <c:if test="${modifyEmployee.manager1.id == u.id}"><c:out value="selected='selected'"/></c:if> >${u.firstName} ${u.lastName} - ${u.email}</option>
						</c:forEach>
					</select> 
					
					<label for="manager2">Manager2</label>
					<select name="manager2" id="manager2">
						<option value="">NONE</option>
						<c:forEach var="u" items="${allusers}">
							<option value="${u.id}" <c:if test="${modifyEmployee.manager2.id == u.id}"><c:out value="selected='selected'"/></c:if> >${u.firstName} ${u.lastName} - ${u.email}</option>
						</c:forEach>
					</select> 
					
					<button type="submit" name="go">Submit</button>
					<button id="cancelButton">Cancel</button>
				</fieldset>
			</form:form>
		</div>
	</div>

	<script src="${resources}/js/uswds.min.js"></script>
	<script src="${resources}/js/jquery-3.1.0.min.js"></script>
	
	<script>
		$(document).ready(function() {

			$("#cancelButton").click(function() {
				window.location.href = "${admin}"
			});

		});
	</script>

</body>
</html>
