<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>

<c:url value="/resources" var="resources" />
<c:url value="/admin/modifyUser" var="modifyUser" />
<c:url value="/admin/createUser" var="createUser" />
<c:url value="/userProfile" var="userProfile" />
<c:url value="/orgchart" var="orgChart" />


<!DOCTYPE html>
<!--[if lt IE 9]><html class="lt-ie9"><![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en">
<!--<![endif]-->
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="HandheldFriendly" content="True">
<meta name="MobileOptimized" content="320">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Admin Page</title>

<!--[if lt IE 9]>
	  <script src="${resources}/js/html5shiv.min.js"></script>
	<![endif]-->

<link href="${resources}/css/uswds.min.css" rel="stylesheet" />
<link href="${resources}/css/kudos.css" rel="stylesheet" />
</head>

<body>
	<a class="skipnav" href="#main-content">Skip to main content</a>
	<header class="usa-site-header" role="banner">
		<div class="site-navbar">
			<a class="menu-btn" href="#">Menu</a>
			<div class="site-logo" id="logo">
				<em>Profile for: ${employee}</em>
			</div>
			<ul class="usa-button-list usa-unstyled-list">
				<li><a class="usa-button usa-button-outline-inverse" onclick="window.location.href = '${userProfile}'"> Back </a></li>
				<li><a class="usa-button usa-button-outline-inverse" onclick="window.location.href = '${logout}'"> Logout </a></li>
			</ul>
		</div>
	</header>
	<div class="main-content usa-center" id="main-content">
		<div class="usa-content">
			<button id="createButton">Create User</button>
			<button id="modifyButton">Modify User</button>
			<button id="orgButton">Org Chart</button>

		</div>
	</div>

	<script src="${resources}/js/uswds.min.js"></script>
	<script src="${resources}/js/jquery-3.1.0.min.js"></script>

	<script>
		$(document).ready(function() {

			$("#modifyButton").click(function() {
				window.location.href = "${modifyUser}"
			});

			$("#createButton").click(function() {
				window.location.href = "${createUser}"
			});
			
			$("#orgButton").click(function() {
				window.location.href = "${orgChart}"
			});


		});
	</script>

</body>
</html>
