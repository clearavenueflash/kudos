<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>

<c:url value="/resources" var="resources" />

<!DOCTYPE html>
<!--[if lt IE 9]><html class="lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--><html lang="en"><!--<![endif]-->
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<!--[if lt IE 9]>
	  <script src="${resources}/js/html5shiv.min.js"></script>
	<![endif]-->
	
	<link href="${resources}/css/kudos.css" rel="stylesheet" />
	
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {packages:["orgchart"]});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Name');
        data.addColumn('string', 'Manager');
        
        <c:forEach var="u" items="${allusers}">
        	data.addRows([[{v:'${u.id}', f:'${u.firstName} ${u.lastName}'}, <c:choose><c:when test="${u.manager1 != null}">'${u.manager1.id}'</c:when><c:otherwise>''</c:otherwise></c:choose>]]);
        </c:forEach>
        
        // Create the chart.
        var chart = new google.visualization.OrgChart(document.getElementById('chart_div'));
        // Draw the chart, setting the allowHtml option to true for the tooltips.
        chart.draw(data, {allowHtml:true});
      }
   </script>
</head>

<body>
	<div class="main-content usa-center" id="main-content">
		<div class="usa-content">
			<header>
				<h1>Org Chart</h1>
			</header>
			
			<div id="chart_div"></div>
		</div>
	</div>

	<script src="${resources}/js/jquery-3.1.0.min.js"></script>

</body>
</html>
