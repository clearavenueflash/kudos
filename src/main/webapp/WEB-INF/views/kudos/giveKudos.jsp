<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>

<c:url value="/resources" var="resources" />
<c:url value="/kudos/processKudos" var="processKudos" />
<c:url value="/userProfile" var="userProfile" />
<c:url value="/logout" var="logout" />



<!DOCTYPE html>
<!--[if lt IE 9]><html class="lt-ie9"><![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en">
<!--<![endif]-->
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="HandheldFriendly" content="True">
<meta name="MobileOptimized" content="320">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Admin Page</title>

<!--[if lt IE 9]>
	  <script src="${resources}/js/html5shiv.min.js"></script>
	<![endif]-->
<link href="${resources}/css/starrating.css" rel="stylesheet">
<link href="${resources}/css/uswds.min.css" rel="stylesheet" />
<link href="${resources}/css/kudos.css" rel="stylesheet" />
</head>

<body>
	<a class="skipnav" href="#main-content">Skip to main content</a>
	<header class="usa-site-header" role="banner">
		<div class="site-navbar">
			<a class="menu-btn" href="#">Menu</a>
			<div class="site-logo" id="logo">
				<em>Kudos Menu</em>
			</div>
			<ul class="usa-button-list usa-unstyled-list">
				<li><a class="usa-button usa-button-outline-inverse" onclick="window.location.href = '${userProfile}'"> Cancel </a></li>
				<li><a class="usa-button usa-button-outline-inverse" onclick="window.location.href = '${logout}'"> Logout </a></li>
			</ul>
		</div>
	</header>

	<div class="main-content" id="main-content">
		<div class="kudos-content usa-content">
			<form:form class="usa-form" method="post" action="${processKudos}" modelAttribute="kudosForm">
				<fieldset>
					<legend>Kudos</legend>
					<label for="forEmployee">For User <span class="usa-additional_text">Required</span></label>
					<select name="forEmployee" id="forEmployee">
						<c:forEach var="u" items="${allusers}">
							<option value="${u.id}">${u.firstName} ${u.lastName}</option>
						</c:forEach>
					</select>
					<label for="projectId">Project</label>
					<select name="projectId" id="projectId">
						<c:forEach var="proj" items="${allProjects}">
							<option value="${proj.id}">${proj.projectName}</option>
						</c:forEach>
					</select>
					<div>
						<br><br>
						Rating:
						<div class="rating">
							<input type="radio" id="rating5" name="rating" value="FIVE"><label class="full" for="rating5"></label>
							<input type="radio" id="rating4" name="rating" value="FOUR"><label class="full" for="rating4"></label>
							<input type="radio" id="rating3" name="rating" value="THREE"><label class="full" for="rating3"></label>
							<input type="radio" id="rating2" name="rating" value="TWO"><label class="full" for="rating2"></label>
							<input type="radio" id="rating1" name="rating" value="ONE"><label class="full" for="rating1"></label>
						</div>
						<input id="rating0" type="radio" name="rating" value="ZERO" hidden="" checked="">
						<label for="rating0" hidden="">0</label>
					</div>
					<label for="kudos">Kudos <span class="usa-additional_text">Required</span></label> 
					<textarea id="kudos" name="kudos" type="text" required="" aria-required="true" maxlength="250"></textarea>
					<button type="submit" name="go">Submit</button>
					<button id="cancelButton">Cancel</button>
				</fieldset>
			</form:form>
		</div>
	</div>

	<script src="${resources}/js/uswds.min.js"></script>
	<script src="${resources}/js/jquery-3.1.0.min.js"></script>
	
	<script>
		$(document).ready(function() {

			$("#cancelButton").click(function() {
				window.location.href = "${userProfile}"
			});
		});
	</script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('input[type=radio]').click(function() {
				if (this.previous) {
					this.checked = false;
					$('#rating0').checked = true;
				}
				this.previous = this.checked;
			});
		});
	</script>
</body>
</html>
