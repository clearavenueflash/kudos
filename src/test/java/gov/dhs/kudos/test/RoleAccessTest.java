package gov.dhs.kudos.test;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import gov.dhs.kudos.SecurityConfiguration;
import gov.dhs.kudos.test.config.TestRootConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestRootConfig.class, SecurityConfiguration.class })
@WebAppConfiguration
public class RoleAccessTest {

	private static final String LOGIN_URL = "http://localhost/login";

	@Autowired
	protected WebApplicationContext context;

	protected MockMvc mockMvc;

	@Before
	public void setup() throws Exception {
		mockMvc = MockMvcBuilders.webAppContextSetup(context).alwaysDo(print()).apply(springSecurity()).build();
	}

	@Test
	@WithAnonymousUser
	public void testIndexAnonymous() throws Exception {
		mockMvc.perform(get("/")).andExpect(status().isOk());
	}

	@Test
	@WithMockUser(roles = "ADMIN")
	public void testIndexAdmin() throws Exception {
		mockMvc.perform(get("/")).andExpect(status().isOk());
	}

	@Test
	@WithMockUser(roles = "EMPLOYEE")
	public void testIndexUser() throws Exception {
		mockMvc.perform(get("/")).andExpect(status().isOk());
	}

	@Test
	@WithMockUser(roles = "SUPERVISOR")
	public void testIndexManager() throws Exception {
		mockMvc.perform(get("/")).andExpect(status().isOk());
	}

	@Test
	@WithAnonymousUser
	public void testUserProfileAnonymousNoLogin() throws Exception {
		mockMvc.perform(get("/userProfile")).andExpect(status().isFound()).andExpect(redirectedUrl(LOGIN_URL));
	}

	@Test
	@WithMockUser(roles = "ADMIN")
	public void testUserProfileAdminNoLogin() throws Exception {
		mockMvc.perform(get("/userProfile")).andExpect(status().isOk());
	}

	@Test
	@WithMockUser(roles = "EMPLOYEE")
	public void testUserProfileUserNoLogin() throws Exception {
		mockMvc.perform(get("/userProfile")).andExpect(status().isOk());
	}

	@Test
	@WithMockUser(roles = "SUPERVISOR")
	public void testUserProfileManagerNoLogin() throws Exception {
		mockMvc.perform(get("/userProfile")).andExpect(status().isOk());
	}

	@Test
	@WithMockUser(roles = "EMPLOYEE")
	public void testEmployeeLanding() throws Exception {
		mockMvc.perform(get("/landing/employee")).andExpect(status().isOk());
	}

	@Test
	@WithMockUser(roles = "SUPERVISOR")
	public void testSupervisorLanding() throws Exception {
		mockMvc.perform(get("/landing/supervisor")).andExpect(status().isOk());
	}

	@Test
	@WithMockUser(roles = "EMPLOYEE")
	public void testSupervisorLandingWithEmployee() throws Exception {
		mockMvc.perform(get("/landing/supervisor")).andExpect(status().isForbidden());
	}

	@Test
	@WithMockUser(roles = "SUPERVISOR")
	public void testEmployeeLandingWithSupervisor() throws Exception {
		mockMvc.perform(get("/landing/employee")).andExpect(status().isForbidden());
	}

}
